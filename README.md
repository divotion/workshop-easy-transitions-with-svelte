# Workshop: Easy transitions in Svelte


## Prerequisites

- NodeJS > v8.x
- NPM > 6.x


## Setup

Make sure you have NodeJS and NPM or Yarn installed so you can get started right away!

Download the repository:

`npx degit git@bitbucket.org:divotion/workshop-easy-transitions-with-svelte.git`

or  

`git clone git@bitbucket.org:divotion/workshop-easy-transitions-with-svelte.git`

Then install the dependencies with `npm install` or `yarn install`.


## Project structure

- `exercises`: Directory containing the exercises.
    - `1-beginner`:  Exercise directory.
        - `README.md`: Explanation and instructions for the exercise.
        - `src`: The source of the exercise.
            - `App.svelte`: The base layout of the exercise,
            - `Exercise.svelte`: This file contains the starting point for the exercise, it will provide you with minimal structure, styles and behavior.
            - `Solution.svelte`: The suggested solution for this exercise; whenever you get stuck, remember to take a peek here.
            - `main.js`: The entry point for the exercise bundle.
- `src`: The shared source for all exercises, here you'll find some components and utility.
- `public`: This is your public directory, whenever you run an exercise this folder will be available from `/`.
    - `global.css`: A file for global CSS styles, since Svelte utilizes CSS scoping we can use this to add some global styles or utility.
    - `index.html`: The base HTML layout used by all exercises.


## The workshop
In this workshop you'll will be making a small application which shows some information about five well known Star 
Wars characters. The structure, logic and styles have been prepared so you can focus on making cool transitions with 
Svelte.


## Starting an exercise

The exercises can be found in the `exercises` directory in the project root, where each exercise has their own 
directory. Each exercise directory contains a `README.md` file which contains the explanation and instructions for 
that exercise. The exercise directory also contains a `src` folder, inside this folder you will find an 
`Exercise.svelte` this is where you'll write the code for the exercise. 

**Beginner**: the recommended starting point if you have little to no experience with svelte or similar frameworks. You'll 
apply a transition to the opening and closing of a pop-up menu.  
For the beginner exercise run: `npm run beginner`

**Intermediate**: if you already have experience with svelte (or similar frameworks) this exercise will prove to be a nice 
challenge. Starting with the menu from the previous exercise you will create a pop up menu which asynchronously 
fetches content.  
For the intermediate exercise run: `npm run intermediate`

**Expert**: the expert exercise is for those looking for a real challenge, you'll create a page transition in a simulated 
SPA environment. You'll have to create a complex transition between elements in separate components.

(It's recommended to at do at least the intermediate exercise first to get an idea of how you can leverage Svelte's API.)

For the expert exercise run: `npm run expert` 


## Useful links

https://bitbucket.org/divotion/workshop-easy-transitions-with-svelte - The URL to this repository.

https://svelte.dev/tutorial/transition - The basics of the Svelte transition API explained; interactive and easy to read and understand.

https://dev.to/buhrmi/svelte-component-transitions-5ie - A tutorial on how to make cool page transitions with Svelte.  

https://svelte.dev/examples#deferred-transitions - Some simple but powerful examples of transitions with Svelte.


## Support
Send your questions to: [meetup@divotion.com](mailto:meetup@divotion.com)

Please provide us with some details about the problem you're encountering, and we will contact you as soon as possible.


## Credits

Paul Martens  
Sibbe Heijne  
Divotion  


## Copyright
Star Wars and all associated names and/or images are copyright Lucasfilm Ltd. Images were freely collected from [Wookiepedia](http://starwars.wikia.com/wiki/Main_Page).