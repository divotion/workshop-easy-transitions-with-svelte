function transform(source) {
    return `export default ${JSON.stringify(source)}`;
}

module.exports = function stringLoader(source) {
    return transform(source);
};