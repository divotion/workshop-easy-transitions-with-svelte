const marked = require('marked');

function transform(source) {
    return `export default ${JSON.stringify(marked(source))}`;
}

module.exports = function markdownLoader(source) {
    return transform(source);
};