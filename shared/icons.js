import arrowLeft from '../public/images/icons/arrowLeft.svg';
import close from '../public/images/icons/close.svg';
import info from '../public/images/icons/info.svg';
import loading from '../public/images/icons/loading.svg';

export default {
    arrowLeft,
    close,
    info,
    loading,
};