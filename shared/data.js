export const characters = [
    {
        id: 2,
        name: 'C-3PO',
        'Birth year': '112BBY',
        'Species': 'Droid',
        'Height': '167cm',
        'Mass': '75kg',
        'Gender': 'n/a',
        'Hair color': 'n/a',
        'Skin color': 'Gold',
        'Homeworld': 'Tatooine',
        quote: {
            content: 'I am C-3PO, human-cyborg relations.',
            reference: 'C-3PO',
        },
    },
    {
        id: 4,
        name: 'Darth Vader',
        'Birth year': '41.9BBY',
        'Species': 'Unknown',
        'Height': '202cm',
        'Mass': '136kg',
        'Gender': 'Male',
        'Hair color': 'None',
        'Skin color': 'White',
        'Homeworld': 'Tatooine',
        quote: {
            content: 'You are the Chosen One. You have brought balance to this world. Stay on this path and you will do it again… for the galaxy. But beware… your heart…',
            reference: 'The Father\'s last words, to Anakin Skywalker',
        },
    },
    {
        id: 10,
        name: 'Obi-Wan Kenobi',
        'Birth year': '57BBY',
        'Species': 'Unknown',
        'Height': '182cm',
        'Mass': '77kg',
        'Gender': 'Male',
        'Hair color': 'Auburn, White',
        'Skin color': 'Fair',
        'Homeworld': 'Stewjan',
        quote: {
            content: 'You\'ve been a good apprentice, Obi-Wan, and you\'re a much wiser man than I am. I foresee you will become a great Jedi Knight.',
            reference: 'Qui-Gon Jinn',
        },
    },
    {
        id: 16,
        name: 'Jabba Desilijic Tiure',
        'Birth year': '600BBY',
        'Species': 'Hutt',
        'Height': '175cm',
        'Mass': '1,358kg',
        'Gender': 'Hermaphrodite',
        'Hair color': 'n/a',
        'Skin color': 'Green-tan, Brown',
        'Homeworld': 'Hal Nutta',
        quote: {
            content: 'If I told you half the things I\'ve heard about this Jabba the Hutt, you\'d probably short-circuit!',
            reference: 'C-3PO',
        },
    },
    {
        id: 20,
        name: 'Yoda',
        'Birth year': '896BBY',
        'Species': 'Yoda\'s Species',
        'Height': '66cm',
        'Mass': '17kg',
        'Gender': 'Male',
        'Hair color': 'White',
        'Skin color': 'Green',
        'Homeworld': 'Unknown',
        quote: {
            content: 'For my ally is the Force, and a powerful ally it is. Life creates it, makes it grow. Its energy surrounds us and binds us. Luminous beings are we, not this crude matter. You must feel the Force around you; here, between you, me, the tree, the rock, everywhere, yes. Even between the land and the ship.',
            reference: 'Yoda, to Luke Skywalker',
        },
    },
];

export function getById(id) {
    return characters.find((c) => c.id === id);
}

export function getCharacterImage(id) {
    return `/images/characters/${id}.jpg`;
}