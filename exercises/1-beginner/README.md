# Workshop: beginners

### The exercise
This exercise focuses on getting familiar with the Svelte transition API. In `Exercise.svelte` you will find a 
menu which opens and closes. In current state it feels abrupt and it doesn't look very appealing, as a solution we 
will apply a transition to the opening and closing of the menu.

Remember applying a transition on an element can be as simple as:

`<div transition:scale></div>`

### Extra challenge
Try to create a natural flow by adding transitions to the content as well. 

Whenever you need a different transition for incoming or outgoing transitions, you can leverage 
the `in`/`out` directives:

`<div in:fade={{ delay: 1000 }} out:fade></div>`