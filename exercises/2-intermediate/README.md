# Workshop: intermediate

### The exercise
In this exercise we will be adding some behavior to the menu we created in the previous exercise. If you skipped the 
previous exercise, don't worry, everything is set up for you to get started and focus on this exercise.

For this exercise you'll need the `crossfade` transition you can either use the `crossfade` transition provided by 
svelte (`import { crossfade } from 'svelte/transitions`), or the adjusted version provided by this project (`import { crossfade } from @/transitions.js`) 

Tip: if you're having trouble applying the `crossfade` transition make sure that all elements are using the same 
instances of the `send` and `receive` functions.
