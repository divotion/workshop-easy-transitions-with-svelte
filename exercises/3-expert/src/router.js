import { writable } from 'svelte/store';
import { getById } from '@/data';

const id = +location.hash.slice(1);
const route = getById(id);

const store = writable({
    route: route ? route.id : null,
    previous: null,
    data: route || null,
});

const setRoute = (route) => (router) => {
    router.previous = router.data;

    if (route) {
        location.hash = route.id;
        router.route = route.id;
        router.data = route;
    } else {
        location.hash = '';
        router.route = null;
        router.data = null;
    }

    return router;
};

export const router = {
    subscribe: store.subscribe,
    goTo(route) {
        store.update(setRoute(route));
    },
};
