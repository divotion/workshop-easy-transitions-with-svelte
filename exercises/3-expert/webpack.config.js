const webpackConfig = require('../../build/webpack.config');

module.exports = webpackConfig({
	context: __dirname,

	resolve: {
		alias: {
			'@exercise': __dirname,
		},
	},
});
