# Workshop: expert

### The exercise
In this final exercise we have extended our app with a small routing system and a separate "page". The layout and 
behavior for this page can be found in `./src/Character.svelte`, when clicking the name of the character in the 
character menu will navigate us to a new page containing some extra information, on this page we can go to the next or 
previous character.

Now it's your job to apply a transition for the opening of the page from the character menu, and then apply a 
transition for when the user navigates to the next or previous character.

This exercises leverages all the tools used in previous exercises in a more complex environment.